# README #

These are the files you need if you want to contribute and add translations for the game Otomaid@cafe Renpy edition. You will need a PO file editor like POEdit to work on them. You can do it with a regular text editor too but tools like POEdit makes it easier.

You can either continue an existing translation by editing the PO file or create a new one using the POT model file.

### Status ###
- **Japanese**: Complete
- **Spanish**: Complete
- **Russian**: In progress
- **French**: In progress

### Contribution guidelines ###

* **DO NOT** use Google Translation for you whole work. Using it time to time if you need help is ok but don't use it for the whole file since its translations can be bad.
* It's not necessary to translate onomatopoeias.
* Keep the japanese " things like `「」` and `『』`

### Credits ###

English translation: Lv1 Translations

Spanish translation: Soulhunter no Fansub

Russian translation: Project Gardares